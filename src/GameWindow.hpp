#pragma once
#include "MapGen.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"

class GameWindow {
	public:
		GameWindow(const char* gameName, const int gameWidth, const int gameHeight);
		sf::RenderWindow* m_pWindow;
		void toggleGame();
		bool isToggled();
		void update(entities* env);
		void draw(entities* env);
		void assignTextures(entities* env);
	private:
		std::map<Textures::ID, std::unique_ptr<sf::Texture>> m_textureMap;
		sf::Clock m_clock;
		sf::Event m_event;
		sf::View m_mainView;
		sf::View m_miniView;
		bool m_gameRunning;
		
		bool createTextures();
		bool loadTexture(Textures::ID id, const std::string& fileName);
		sf::Texture& getTexture(Textures::ID id);
};
