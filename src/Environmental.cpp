#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"

Environmental::Environmental(std::string entName, int x, int y, Textures::ID texID, bool isWall) : Entity(entName, x, y, texID)
{
	m_xCord = x;
	m_yCord = y;
	m_entName = entName;
	m_isWall = isWall;
}

bool Environmental::getIsWall()
{
	return m_isWall;
}
