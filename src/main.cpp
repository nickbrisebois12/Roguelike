#include "Game.hpp"

const char* GameName = "Roguelike";
const int GameWidth  = 768;
const int GameHeight = 576;

int main(int argc, char* argv[])
{
	Game* pGame = new Game(GameName, GameWidth, GameHeight);
	pGame->start();
}
