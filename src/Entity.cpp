#include <SFML/Graphics.hpp>
#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"

Entity::Entity(std::string entName, int x, int y, Textures::ID texID)
{
	m_xCord = x;
	m_yCord = y;
	m_entName = entName;
	m_texID = texID;
}

Textures::ID Entity::getTexID()
{
	return m_texID;
}

void Entity::setEnt(Textures::ID texID)
{
	m_texID = texID;
}

void Entity::setSprite(sf::Sprite newSprite)
{
	m_sprite = newSprite;
}

sf::Sprite* Entity::getSprite()
{
	return &m_sprite;
}
