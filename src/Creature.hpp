#pragma once

#include "Environmental.hpp"

class Creature: public Entity
{
	public:
		Creature(std::string entName, int x, int y, Textures::ID texID);
		virtual bool move(int dir, Environmental***& enviro);
		virtual void handleInput(Environmental***& enviro);
		virtual void updateSprite();
	protected:
		int m_health;
};
