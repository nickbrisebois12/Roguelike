#include <SFML/Graphics.hpp>
#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"
#include "Creature.hpp"

Creature::Creature(std::string entName, int x, int y, Textures::ID texID) : Entity(entName, x, y, texID)
{
	m_health = 100;
}

bool Creature::move(int dir, Environmental***& enviro)
{
	switch(dir) {
		// Up
		case 1:
			if(!enviro[m_yCord + 1][m_xCord]->getIsWall()) {
				m_yCord += 1;
			}
			break;
		// Right
		case 2:
			if(!enviro[m_yCord][m_xCord + 1]->getIsWall()) {
				m_xCord += 1;
			}
			break;
		// Down
		case 3:
			if(!enviro[m_yCord - 1][m_xCord]->getIsWall()) {
				m_yCord -= 1;
			}
			break;
		// Left
		case 4:
			if(!enviro[m_yCord][m_xCord - 1]->getIsWall()) {
				m_xCord -= 1;
			}
			break;
		default:
			std::cout << "Unrecognized move direction" << std::endl;
			return false;
	}
	updateSprite();
	return true;
}

void Creature::updateSprite()
{
	m_sprite.setPosition(m_xCord * 32, m_yCord * 32);
}

void Creature::handleInput(Environmental***& enviro)
{

}
