#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"
#include "Creature.hpp"
#include "Player.hpp"
#include "GameWindow.hpp"
#include "MapGen.hpp"

MapGen::MapGen(int gameWidth, int gameHeight, int tileSize)
{
	srand(time(NULL));
	m_gameWidth = gameWidth;
	m_gameHeight = gameHeight;
	m_tileSize = tileSize;
	m_tilesHeight = floor(m_gameHeight / m_tileSize);
	m_tilesWidth = floor(m_gameWidth / m_tileSize);
	createMap();
	createCreatures();
}

enviroMeta MapGen::setupEnvironment()
{
	enviroMeta meta;
	Environmental*** pNewEnvironment = new Environmental**[m_tilesHeight];
	for(int y = 0; y < m_tilesHeight; y++){
		pNewEnvironment[y] = new Environmental*[m_tilesWidth];
		for(int x = 0; x < m_tilesWidth; x++) {
			if(m_gameMap[y][x] == Textures::Wall) {
				pNewEnvironment[y][x] = new Environmental("Test", x, y, m_gameMap[y][x], true);
			} else {
				pNewEnvironment[y][x] = new Environmental("Test", x, y, m_gameMap[y][x], false);
			}

			sf::Sprite newSprite;
			newSprite.setPosition(x * m_tileSize, y * m_tileSize);

			pNewEnvironment[y][x]->setSprite(newSprite);
		}
	}
	meta.pEntity = pNewEnvironment;
	meta.width = m_tilesWidth;
	meta.height = m_tilesHeight;
	return meta;
}

creaturesMeta MapGen::setupCreatures()
{
	creaturesMeta meta;
	Creature*** pNewCreatures = new Creature**[m_tilesHeight];
	for(int y = 0; y < m_tilesHeight; y++) {
		pNewCreatures[y] = new Creature*[m_tilesWidth];
		for(int x = 0; x < m_tilesWidth; x++) {
			if(m_crMap[y][x] == Textures::MushroomMan) {
				pNewCreatures[y][x] = new Player("Player", x, y, m_crMap[y][x]);

				sf::Sprite newSprite;
				newSprite.setPosition(x * m_tileSize, y * m_tileSize);

				pNewCreatures[y][x]->setSprite(newSprite);
			}else {
				pNewCreatures[y][x] = NULL;
			}
		}
	}
	meta.pEntity = pNewCreatures;
	meta.width = m_tilesWidth;
	meta.height = m_tilesHeight;
	return meta;
}


/**
 * createMap()
 * Creates a randomly generated map represented as a Textures::ID array
 */
void MapGen::createMap()
{
	sf::Vector2i walker;
	walker.x = floor(m_tilesWidth / 2);
	walker.y = floor(m_tilesHeight / 2);

	// Fill map array with blank tiles
	for(int y = 0; y < m_tilesHeight; y++) {
		for(int x = 0; x < m_tilesWidth; x++) {
			m_gameMap[y][x] = Textures::Wall;
		}
	}

	for(int i = 0; i < 100; i++){
		if(randBetween(0, 1) == 0)
			walker = createRoom(walker, randBetween(1, 2), randBetween(2, 3), true);
		else
			walker = createRoom(walker, randBetween(2, 3), randBetween(1, 2), false);
	}
}

/**
 * createRoom(Start Position, Room Width, Room Height, Is Moving Upwards)
 * Creates a room out of floor tiles
 * Returns the location to continue building the map off of
 */
sf::Vector2i MapGen::createRoom(sf::Vector2i startPos, int width, int height, bool moveUp)
{
	std::vector<sf::Vector2i> possibleExit;
	for(int y = 0; y < height; y++) {
		for(int x = 0; x < width; x++) {
			sf::Vector2i newPos = startPos;
			int addX = x;
			int addY = y;
			if(moveUp) {
				addX = -addX;
				addY = -addY;
			}
			newPos.x = startPos.x + addX;
			newPos.y = startPos.y + addY;
			if(walkValid(newPos, addX, addY)) {
				m_gameMap[newPos.y][newPos.x] = Textures::Floor;
				if((newPos.y == startPos.y + height-1 || newPos.y == startPos.y) 
						|| (newPos.x == startPos.x + width-1 || newPos.x == startPos.x)) {
					possibleExit.push_back(sf::Vector2i(newPos.x, newPos.y));
				}
			}
		}
	}
	int randExitPoint = 0;
	if(possibleExit.size() > 0)
		randExitPoint = randBetween(0, possibleExit.size()-1);
	return possibleExit[randExitPoint];
}

void MapGen::createCreatures()
{
	bool playerMade = false;
	for(int y = 0; y < m_tilesHeight; y++) {
		for(int x = 0; x < m_tilesWidth; x++) {
			if(m_gameMap[y][x] == Textures::Floor && !playerMade) {
				m_crMap[y][x] = Textures::MushroomMan;
				playerMade = true;
			} else {
				m_crMap[y][x] = Textures::None;
			}
		}
	}
}

bool MapGen::walkValid(sf::Vector2i startPos, int addX, int addY)
{
	// Make sure next map gen walk is inside the game map
	bool isValid = (startPos.y + addY < m_tilesHeight) && (startPos.y + addY > 0);
	isValid = isValid && (startPos.x + addX < m_tilesWidth) && (startPos.x + addX > 0);
	return isValid;
}

int MapGen::randBetween(int min, int max)
{
	return rand()%(max-min + 1) + min;
}
