/*  PROGRAM: Roguelike Game Engine
 *  AUTHOR: Nick Brisebois
 *  DATE: Wednesday, August 1, 2018
 *  PURPOSE: Quickly and efficiently causes a segmentation fault
 */

#include "Game.hpp"

Game::Game(const char* gameName, const int gameWidth, const int gameHeight)
{
	pGameWindow = new GameWindow(gameName, gameWidth, gameHeight);
	this->gameWidth = gameWidth;
	this->gameHeight = gameHeight;
	this->tileSize = 32;
}

void Game::start()
{
	MapGen* mapGen = new MapGen(gameWidth, gameHeight, tileSize);
	entities env;

	env.enviro = mapGen->setupEnvironment();
	env.creatures = mapGen->setupCreatures();
	pGameWindow->assignTextures(&env);

	while(pGameWindow->isToggled()) {
		pGameWindow->update(&env);
		pGameWindow->draw(&env);
	}
}

