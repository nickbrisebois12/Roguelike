#include <SFML/Graphics.hpp>
#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"
#include "Creature.hpp"
#include "Player.hpp"

void Player::handleInput(Environmental***& enviro)
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		move(3, enviro);
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		move(2, enviro);
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		move(1, enviro);
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		move(4, enviro);
	}
}
