#pragma once

class Entity 
{
	public:
		Entity(std::string entName, int x, int y, Textures::ID texID);
		Textures::ID getTexID();
		void setEnt(Textures::ID texID);
		void setSprite(sf::Sprite newSprite);
		sf::Sprite* getSprite();
		int m_xCord;
		int m_yCord;
	protected:
		Textures::ID m_texID;
		std::string m_entName;
		sf::Sprite m_sprite;
};
