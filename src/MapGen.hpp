#pragma once

struct enviroMeta {
	Environmental*** pEntity;
	int height;
	int width; };

struct creaturesMeta {
	Creature*** pEntity;
	int height;
	int width;
};

struct entities {
	enviroMeta enviro;
	creaturesMeta creatures;
};

class MapGen {
	public:
		MapGen(int gameWidth, int gameHeight, int tileSize);
		enviroMeta setupEnvironment();
		creaturesMeta setupCreatures();
	private:
		int m_gameWidth;
		int m_gameHeight;
		int m_tilesHeight;
		int m_tilesWidth;
		int m_tileSize;
		Textures::ID m_gameMap[18][24];
		Textures::ID m_crMap[18][24];
		void createMap();
		void createCreatures();
		sf::Vector2i createRoom(sf::Vector2i startPos, int width, int height, bool moveUp);
		bool walkValid(sf::Vector2i startPos, int addX, int addY);
		sf::Vector2i horTunnel(sf::Vector2i startPos, int width);
		int randBetween(int min, int max);
};
