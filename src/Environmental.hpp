#pragma once
#include <SFML/Graphics.hpp>

class Environmental: public Entity
{
	public:
		Environmental(std::string entName, int x, int y, Textures::ID texID, bool isWall);
		bool getIsWall();
	protected:
		bool m_isWall;
};
