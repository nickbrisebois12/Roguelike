#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Environmental.hpp"
#include "Creature.hpp"
#include "Player.hpp"
#include "GameWindow.hpp"
#include "MapGen.hpp"

class Game {
	public:
		Game(const char* gameName, const int gameWidth, const int gameHeight);
		int gameWidth; 
		int gameHeight;
		void start();
	private:
		sf::Sprite* pToDraw;
		GameWindow* pGameWindow;
		int tileSize;

		Player* createPlayer(std::string playerName, int x, int y, int texID);
		enviroMeta setupEnvironment();
		creaturesMeta setupCreatures();

};
