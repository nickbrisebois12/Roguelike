#include <SFML/Graphics.hpp>
#include <iostream>

#include "types.hpp"
#include "Entity.hpp"
#include "Creature.hpp"
#include "Player.hpp"
#include "Environmental.hpp"
#include "GameWindow.hpp"

GameWindow::GameWindow(const char* gameName, const int gameWidth, const int gameHeight)
{
	m_pWindow = new sf::RenderWindow(sf::VideoMode(gameWidth, gameHeight), gameName);
	m_pWindow->setFramerateLimit(60);
	createTextures();
	m_mainView = sf::View(sf::FloatRect(200, 200, 300, 200));
	m_gameRunning = true;
}

void GameWindow::toggleGame()
{
	m_gameRunning = !m_gameRunning;
}

bool GameWindow::isToggled()
{
	return m_gameRunning && m_pWindow->isOpen();
}

void GameWindow::assignTextures(entities* env)
{
	for(int y = 0; y < env->enviro.height; y++) {
		for(int x = 0; x < env->enviro.width; x++ ){
			if(env->enviro.pEntity[y][x]->getTexID() != Textures::None) {
				env->enviro.pEntity[y][x]->getSprite()->setTexture(getTexture(env->enviro.pEntity[y][x]->getTexID()));
			}
		}
	}

	for(int y = 0; y < env->creatures.height; y++) {
		for(int x = 0; x < env->creatures.width; x++ ){
			if(env->creatures.pEntity[y][x] != NULL) {
				env->creatures.pEntity[y][x]->getSprite()->setTexture(getTexture(env->creatures.pEntity[y][x]->getTexID()));
			}
		}
	}
}

void GameWindow::update(entities* env)
{
	while(m_pWindow->pollEvent(m_event)) {
		if(m_event.type == sf::Event::Closed) {
			m_pWindow->close();
		}

		if(m_event.type == sf::Event::KeyPressed) {
			for(int y = 0; y < env->creatures.height; y++) {
				for(int x = 0; x < env->creatures.width; x++) {
					if(env->creatures.pEntity[y][x] != NULL) {
						env->creatures.pEntity[y][x]->handleInput(env->enviro.pEntity);
					}
				}
			}
		}
	}
}

void GameWindow::draw(entities* env)
{
	m_pWindow->clear();

	for(int y = 0; y < env->enviro.height; y++) {
		for(int x = 0; x < env->enviro.width; x++ ){
			if(env->enviro.pEntity[y][x]->getTexID() != Textures::None) {
				m_pWindow->draw(*env->enviro.pEntity[y][x]->getSprite());
			}
		}
	}

	for(int y = 0; y < env->creatures.height; y++) {
		for(int x = 0; x < env->creatures.width; x++ ){
			if(env->creatures.pEntity[y][x] != NULL) {
				m_pWindow->draw(*env->creatures.pEntity[y][x]->getSprite());
			}
		}
	}
	m_pWindow->display();
}

bool GameWindow::createTextures()
{
	loadTexture(Textures::Floor, "texture/32x32/GreenFloorTexture32.png");
	loadTexture(Textures::Wall, "texture/32x32/mossywall.png");
	loadTexture(Textures::MushroomMan, "texture/32x32/Mushroomman32.png");
	return true;
}

bool GameWindow::loadTexture(Textures::ID id, const std::string& fileName)
{
	std::unique_ptr<sf::Texture> texture(new sf::Texture());
	if(!texture->loadFromFile(fileName)) {
		return false;
	}
	m_textureMap.insert(std::make_pair(id, std::move(texture)));
	return true;
}

sf::Texture& GameWindow::getTexture(Textures::ID id)
{
	auto found = m_textureMap.find(id);
	return *found->second;
}
