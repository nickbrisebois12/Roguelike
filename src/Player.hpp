#pragma once

class Player: public Creature
{
	public:
		using Creature::Creature;
		virtual void handleInput(Environmental***& enviro);
};
